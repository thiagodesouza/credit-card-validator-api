﻿using Microsoft.AspNetCore.Mvc;
using TS.CreditCardValidator.Core.Models;

namespace TS.CreditCardValidator.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValidateController : ControllerBase
    {
        // GET api/validate
        [HttpGet]
        public CreditCard Get(string number) => new CreditCard(number);
    }
}